//
//  ServicesAssembly.swift
//  Translator
//
//  Created by Qurban on 30.04.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation
protocol IServicesAssembley {
    var languagesService: ILanguagesService {get}
}
class ServicesAssembley: IServicesAssembley {
    lazy var languagesService: ILanguagesService = LanguagesService()
}
