//
//  LanguagesService.swift
//  Translator
//
//  Created by Qurban on 29.04.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

protocol ILanguagesService {
    func saveLanguages()
}

class LanguagesService: ILanguagesService {
    var networkManager: INetworkManager = NetworkManager()
    var storageManager: IStorageManager = StorageManager()

    func saveLanguages() {
        let urlString = "https://translate.yandex.net/api/v1.5/tr.json/getLangs?key=trnsl.1.1.20190212T173411Z.1083337deb465051.c2a1c485b818da2f0b0600fdf09428f88229775e&ui=ru"
        guard let url = URL(string: urlString) else {return }
        let urlRequest = URLRequest(url: url)
        networkManager.perform(request: urlRequest) { (result: Result<LanguagesResponse>) in
            switch result {
            case .succes(let languageResponse):
                    for (key, value) in languageResponse.langs {
                        self.storageManager.updateLanguage(name: value, code: key)
                }
            case .error:
                print ("asdasd")
            }
        }
    }
}
