//
//  TranslateService.swift
//  Translator
//
//  Created by Qurban on 02.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation
import CoreData

protocol ITranslateService {
    func translateWords(text: String,
                        fromCode: String,
                        toCode: String,
                        completion: @escaping (TranslateResponse) -> Void)
    func change(word: Word)
    func delete(word: Word)
}

class TranslateService: ITranslateService {
    var networkManager: INetworkManager = NetworkManager()
    var storageManager: IStorageManager = StorageManager()

    func translateWords(text: String, fromCode: String,
                        toCode: String,
                        completion: @escaping (TranslateResponse) -> Void) {
        let baseUrl = "https://translate.yandex.net/api/v1.5/tr.json/translate"
        let urlString = baseUrl + "?key=trnsl.1.1.20190212T173411Z.1083337deb465051.c2a1c485b818da2f0b0600fdf09428f88229775e&text=\(text)&lang=\(fromCode)-\(toCode)"
        guard let url = URL(string: urlString) else { return }
        let urlRequest = URLRequest(url: url)

        networkManager.perform(request: urlRequest) {[weak self] (result: Result<TranslateResponse>) in

            switch result {

            case .succes(let translateResponse):
                guard let translatedText = translateResponse.text.first else {return}

                self?.storageManager.findOrInsertWord(from: text, to: translatedText, completion: { (_) in
                    completion(translateResponse)
                })
            case .error:
                print("Error TranslateService")
            }
        }
    }

    func delete(word: Word) {
        if let from = word.from {
            storageManager.deleteWord(from: from)
        }
    }

    func change(word: Word) {
        if let from = word.from {
            storageManager.update(from: from)
        }
    }
}
