//
//  TranslateResponse.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

struct TranslateResponse: Codable {
    var lang: String
    var text: [String]
}
