//
//  WordModel.swift
//  Translator
//
//  Created by Qurban on 17.08.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

class WordModel {
    var from: String?
    var to: String?
    var isSaved: Bool?

    init(word: Word) {
        from = word.from
        to = word.to
        isSaved = word.isSaved
    }
}
