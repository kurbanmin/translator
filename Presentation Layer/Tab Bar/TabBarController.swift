//
//  TabBarController.swift
//  Translator
//
//  Created by Qurban on 30.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let wordsListModule = WordsListAssembly().createModule()
        let wordsListNC = UINavigationController(rootViewController: wordsListModule)
        wordsListModule.tabBarItem.image = UIImage(named: "1")
        wordsListModule.tabBarItem.title = "Все слова"

        let translateModule = TranslateAssembly().createModule()
        let translateNC = UINavigationController(rootViewController: translateModule)
        translateModule.tabBarItem.image = UIImage(named: "2")
        translateModule.tabBarItem.title = "Перевод"

        let learnWordsModule = LearnWordsAssembly().createModule()
        let learnWordsNC = UINavigationController(rootViewController: learnWordsModule)
        learnWordsModule.tabBarItem.image = UIImage(named: "3")
        learnWordsModule.tabBarItem.title = "Учить"

        self.viewControllers = [wordsListNC, translateNC, learnWordsNC]

        self.tabBar.barTintColor = UIColor(displayP3Red: 56/255, green: 81/255, blue: 120/255, alpha: 1)
        self.tabBar.tintColor = .white

        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 56/255,
                                                            green: 81/255,
                                                            blue: 120/255, alpha: 1)
        UINavigationBar.appearance().isTranslucent = false
    }
}
