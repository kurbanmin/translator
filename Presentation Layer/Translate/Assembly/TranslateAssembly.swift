//
//  TranslateAssembly.swift
//  Translator
//
//  Created by Qurban on 02.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class TranslateAssembly {
    func createModule() -> TranslateViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "TranslateViewController"
            ) as? TranslateViewController
        let presenter = TranslatePresenter()
        let router = TranslateRouter()

        let languagesService: ILanguagesService = LanguagesService()
        let translateService: ITranslateService = TranslateService()
        let interactor = TranslateInteractor()

        presenter.interactor = interactor
        presenter.router = router
        presenter.view = view

        interactor.presenter = presenter
        interactor.languagesService = languagesService
        interactor.translateService = translateService
        router.transitionHandler = view
        view?.output = presenter

        return view ?? TranslateViewController()
    }
}
