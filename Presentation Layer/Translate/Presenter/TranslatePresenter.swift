//
//  TranslatePresenter.swift
//  Translator
//
//  Created by Qurban on 02.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

class TranslatePresenter {
    weak var view: TranslateViewController!
    var router: TranslateRouterInput!
    var interactor: TranslateInteractor!
    var code = Code(from: "en", to: "ru")
    weak var input: TranslateViewInput?
    var word: Word?
}

extension TranslatePresenter: TranslateViewOutput {
    func didClickFromButton() {
        router.openChooseLanguageModule(direction: .from, moduleOutput: self)
    }

    func didClickToButton() {
        router.openChooseLanguageModule(direction: .to, moduleOutput: self)
    }

    func loadLanguages() {
        interactor.loadLanguages()
    }

    func translateWord(text: String) {
        interactor.translateWord(fromCode: code.from, toCode: code.to, text: text) { [weak self] (translateResponse) in
            var translatedText = ""
            for text in translateResponse.text {
                translatedText += text
            }
            self?.view.set(word: translatedText)
            self?.word?.from = text
            if let to = translateResponse.text.first {
                self?.word?.to = to
            }
        }
    }
}

extension TranslatePresenter: LanguagesModuleOutput {
    func didSelect(language: Language, direction: LanguageDirection) {
        guard let code = language.code else {return}
        switch direction {
        case .from:
            view.setFrom(language: language)
            self.code.from = code
        case .to:
            view.setTo(language: language)
            self.code.to = code
        }
    }
}
