//
//  TranslateViewController.swift
//  Translator
//
//  Created by Qurban on 14.05.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

enum LanguageDirection {
    case from
    case to
}
struct Code {
    var from: String
    var to: String
}

class TranslateViewController: UIViewController {
    var output: TranslateViewOutput!
    var direction: LanguageDirection = .from

    @IBOutlet weak var fromButton: UIButton!
    @IBOutlet weak var toButton: UIButton!

    @IBOutlet weak var swapLanguagesButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var translatedTextView: UITextView!
    @IBOutlet weak var translateTF: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        fromButton.titleLabel?.text = "Русский"
        toButton.titleLabel?.text = "Английский"
        output.loadLanguages()
        translateTF.delegate = self
        translateTF.placeholder = "Введите слово"
    }

    @IBAction func fromAction(_ sender: Any) {
        output.didClickFromButton()
    }

    @IBAction func toAction(_ sender: Any) {
        output.didClickToButton()
    }

    @IBAction func swapAction(_ sender: Any) {
    }

}

extension TranslateViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = translateTF.text {
            output.translateWord(text: text)
        }
        return true
    }
}

extension TranslateViewController: TranslateViewInput {
    func set(word: String) {
        DispatchQueue.main.async {
            self.translatedTextView.text = word
        }
    }

    func setFrom(language: Language) {
        fromButton.titleLabel?.text = language.name
    }

    func setTo(language: Language) {
        toButton.titleLabel?.text = language.name
    }
}
