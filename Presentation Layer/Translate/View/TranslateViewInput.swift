//
//  TranslateViewInput.swift
//  Translator
//
//  Created by Qurban on 26.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

protocol TranslateViewInput: class {
    func setFrom(language: Language)
    func setTo(language: Language)
    func set(word: String)
}
