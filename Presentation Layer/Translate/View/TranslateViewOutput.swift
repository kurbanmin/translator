//
//  TranslateViewOutput.swift
//  Translator
//
//  Created by Qurban on 26.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

protocol TranslateViewOutput {
    func didClickFromButton()
    func didClickToButton()
    func loadLanguages()
    func translateWord(text: String)
}
