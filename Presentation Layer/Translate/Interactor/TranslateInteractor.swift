//
//  LanguagesModel.swift
//  Translator
//
//  Created by Qurban on 14.05.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

class TranslateInteractor {
    weak var presenter: TranslatePresenter!
    var languagesService: ILanguagesService!
    var translateService: ITranslateService!

    func loadLanguages() {
        languagesService.saveLanguages()
    }

    func translateWord(fromCode: String, toCode: String,
                       text: String,
                       completion: @escaping (TranslateResponse) -> Void) {
        self.translateService.translateWords(text: text, fromCode: fromCode, toCode: toCode) { (translateResponse) in
            DispatchQueue.main.async {
                completion(translateResponse)
            }
        }
    }
}
