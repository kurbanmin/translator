//
//  TranslateRouter.swift
//  Translator
//
//  Created by Qurban on 02.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class TranslateRouter: TranslateRouterInput {
    weak var transitionHandler: TranslateViewController!

    func openChooseLanguageModule(direction: LanguageDirection, moduleOutput: LanguagesModuleOutput) {
        let languagesModule = LanguagesAssembly().createModule(direction: direction, moduleOutput: moduleOutput)
        transitionHandler.present(languagesModule, animated: true, completion: nil)
    }
}
