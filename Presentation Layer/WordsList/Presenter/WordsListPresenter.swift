//
//  Presenter.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation
class WordsListPresenter: WordsListOutput {

    weak var view: WordsListViewController!
    var router: WordsListRouter!
    var interactor: WordsListInteractor!

    func change(word: Word) {
        interactor.change(word: word)
    }

    func delete(word: Word) {
        interactor.delete(word: word)
    }
}
