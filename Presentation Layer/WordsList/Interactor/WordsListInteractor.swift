//
//  WordsListInteractor.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation
class WordsListInteractor: WordsListInteractorInput {

    weak var presenter: WordsListPresenter!
    var translateService: ITranslateService!

    func change(word: Word) {
        self.translateService.change(word: word)
    }

    func delete(word: Word) {
        self.translateService.delete(word: word)
    }
}
