//
//  WordsListInput.swift
//  Translator
//
//  Created by Qurban on 01.08.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

protocol WordsListinput: class {
    func change(word: Word)
    func delete(word: Word)
}
