//
//  WordsListDataSource.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit
import CoreData

class WordsListDataSource: NSObject, WordsListCellOutput {

    weak var input: WordsListinput?
    var tableView: UITableView
    var frc: NSFetchedResultsController<Word>!

    var coreDataStack = CoreDataStack.shared

    func change(word: Word) {
        input!.change(word: word)
    }

    init(tableView: UITableView ) {
        self.tableView = tableView
        super.init()

        tableView.register(UINib(nibName: "WordsListCell", bundle: nil), forCellReuseIdentifier: "WordsListCell")
        tableView.dataSource = self

        let fetchRequest: NSFetchRequest<Word> = Word.fetchRequest()

        let nameSortDescription = NSSortDescriptor(key: "from", ascending: true)

        fetchRequest.sortDescriptors = [nameSortDescription]

        self.frc = NSFetchedResultsController<Word>(fetchRequest: fetchRequest,
                                                    managedObjectContext: coreDataStack.mainContext,
                                                    sectionNameKeyPath: nil,
                                                    cacheName: nil)

        frc.delegate = self

        do {
            try frc.performFetch()
        } catch {
            print(error)
        }
    }

}

extension WordsListDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = self.frc.sections else {
            fatalError()
        }

        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.frc.sections else {
            fatalError("no section")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let word = self.frc.object(at: indexPath)

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WordsListCell") as? WordsListCell
            else {return UITableViewCell()}
        cell.word = word
        cell.delegate = self

        return cell
    }

    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let word = self.frc.object(at: indexPath)
            input?.delete(word: word)
        }
    }
}

extension WordsListDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension WordsListDataSource: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any, at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {

        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        }
    }
}
