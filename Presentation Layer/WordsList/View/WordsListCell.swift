//
//  WordsListCell.swift
//  Translator
//
//  Created by Qurban on 25.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

protocol WordsListCellOutput: class {
    func change(word: Word)
}

class WordsListCell: UITableViewCell {

    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var deleteWordButton: UIButton!

    weak var delegate: WordsListCellOutput?

    var word: Word? {
        didSet {
            guard let word = word else {return }
            fromLabel.text = word.from
            toLabel.text = word.to
            if  word.isSaved {
                deleteWordButton.tintColor = UIColor.yellow
            } else {
                deleteWordButton.tintColor = UIColor.black
            }
        }
    }

    @IBAction func deleteWordAction(_ sender: Any) {
        if let word = word {
            delegate?.change(word: word)
        }
    }
}
