//
//  WordsListViewController.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class WordsListViewController: UIViewController, WordsListinput {

    var output: WordsListOutput!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var countWordsButton: UIBarButtonItem!

    func delete(word: Word) {
        output.delete(word: word)
    }

    func change(word: Word) {
        output.change(word: word)
    }

    var dataSource: WordsListDataSource!

    func change(wordModel: WordModel) {

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = WordsListDataSource(tableView: tableView)
        dataSource.input = self
    }

    @IBAction func editAction(_ sender: Any) {
    }
}
