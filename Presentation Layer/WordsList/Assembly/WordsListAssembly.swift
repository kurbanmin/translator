//
//  WorsListAssembly.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class WordsListAssembly {

    func createModule() -> WordsListViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "WordsListViewController"
            ) as? WordsListViewController
        let presenter = WordsListPresenter()
        let router = WordsListRouter()

        let translateService: ITranslateService = TranslateService()
        let interactor = WordsListInteractor()

        interactor.translateService = translateService
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = view

        interactor.presenter = presenter
        router.transitionHandler = view
        view?.output = presenter

        return view ?? WordsListViewController()
    }
}
