//
//  LearnWordsInteractor.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

class LearnWordsInteractor: LearnWordsInteractorInput {
    var translateService: ITranslateService!
    weak var presenter: LearnWordsPresenter!

    func change(word: Word) {
        translateService.change(word: word)
    }
}
