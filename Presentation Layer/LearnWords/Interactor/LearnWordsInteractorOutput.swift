//
//  LearnWordsInteractorOutput.swift
//  Translator
//
//  Created by Qurban on 19.08.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

protocol LearnWordsInteractorInput {
    func change(word: Word)
}
