//
//  LearnWordsPresenter.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

class LearnWordsPresenter: LearnWordsOutput {
    weak var view: LearnWordsViewController!
    var router: LearnWordsRouter!
    var interactor: LearnWordsInteractorInput!

    func change(word: Word) {
        interactor.change(word: word)
    }
}

extension LearnWordsPresenter: LearnWordsInteractorOutput {
}
