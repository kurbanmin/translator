//
//  LearnWordsAssembly.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class LearnWordsAssembly {
    func createModule() -> LearnWordsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "LearnWordsViewController"
            ) as? LearnWordsViewController

        let presenter = LearnWordsPresenter()
        let router = LearnWordsRouter()
        let translateService: ITranslateService = TranslateService()
        let interactor = LearnWordsInteractor()

        presenter.interactor = interactor
        presenter.router = router
        presenter.view = view

        interactor.presenter = presenter
        interactor.translateService = translateService
        router.transitionHandler = view
        view?.output = presenter

        return view ?? LearnWordsViewController()
    }
}
