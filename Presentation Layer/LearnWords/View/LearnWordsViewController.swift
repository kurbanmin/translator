//
//  LearnWordsViewController.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class LearnWordsViewController: UIViewController, LearnWordsInput {
    func change(word: Word) {
        output.change(word: word)
    }

    @IBOutlet weak var tableView: UITableView!

    var output: LearnWordsPresenter!
    var dataSource: LearnWordsDataSource!
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = LearnWordsDataSource(tableView: tableView)
        dataSource.input = self
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
