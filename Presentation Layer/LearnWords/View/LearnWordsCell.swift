//
//  LearnWordsCell.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

protocol LearnWordsCellOutput: class {
    func change(word: Word)
}

class LearnWordsCell: UITableViewCell {
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var persentLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!

    weak var delegate: LearnWordsCellOutput?

    var word: Word? {
        didSet {
            guard let word = word else { return }
            wordLabel.text = word.from
            if word.isSaved {
                deleteButton.tintColor = UIColor.yellow

            } else {
                deleteButton.tintColor = UIColor.black
            }
        }
    }

    @IBAction func deleteAction(_ sender: Any) {
        if let word = word {
            delegate?.change(word: word)
        }
    }

}
