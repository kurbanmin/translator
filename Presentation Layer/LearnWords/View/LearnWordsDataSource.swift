//
//  LearnWordsDataSource.swift
//  Translator
//
//  Created by Qurban on 31.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit
import CoreData

class LearnWordsDataSource: NSObject, LearnWordsCellOutput {
    var tableView: UITableView
    var frc: NSFetchedResultsController<Word>!
    var coreDataStack = CoreDataStack.shared

    weak var input: LearnWordsInput?

    init(tableView: UITableView ) {

        self.tableView = tableView
        super.init()

        tableView.register(UINib(nibName: "LearnWordsCell", bundle: nil), forCellReuseIdentifier: "LearnWordsCell")
        tableView.dataSource = self

        let fetchRequest: NSFetchRequest<Word> = Word.fetchRequestSavedWords()

        let nameSortDescription = NSSortDescriptor(key: "from", ascending: true)
        fetchRequest.sortDescriptors = [nameSortDescription]
        self.frc = NSFetchedResultsController<Word>(fetchRequest: fetchRequest,
                                                    managedObjectContext: coreDataStack.mainContext,
                                                    sectionNameKeyPath: nil,
                                                    cacheName: nil)

        frc.delegate = self
        do {
            try frc.performFetch()

        } catch {
            print(error)
        }
    }

    func change(word: Word) {
        input?.change(word: word)
    }
}

extension LearnWordsDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.frc.sections else {
            fatalError("no section")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let word = self.frc.object(at: indexPath)

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LearnWordsCell") as? LearnWordsCell
            else {return UITableViewCell()}

        cell.word = word
        cell.delegate = self

        return cell
    }
}

extension LearnWordsDataSource: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any, at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {

        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        }
    }
}
