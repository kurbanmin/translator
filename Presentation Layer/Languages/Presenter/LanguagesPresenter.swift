//
//  LanguagesPresenter.swift
//  Translator
//
//  Created by Qurban on 02.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

class LanguagesPresenter {
    weak var interactor: LanguagesInteractor?
    var router: LanguagesRouter!
    var view: LanguagesViewController!
    weak var moduleOutput: LanguagesModuleOutput!

    var direction: LanguageDirection!
}

extension LanguagesPresenter: LanguagesViewOutput {
    func didSelect(_ language: Language) {
        moduleOutput.didSelect(language: language, direction: direction)
        router.closeCurrentModule()
    }
}
