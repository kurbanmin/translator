//
//  LanguagesAssembly.swift
//  Translator
//
//  Created by Qurban on 02.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class LanguagesAssembly {

    func createModule(direction: LanguageDirection,
                      moduleOutput: LanguagesModuleOutput
        ) -> LanguagesViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "LanguagesViewController"
            ) as? LanguagesViewController

        let presenter = LanguagesPresenter()
        let router = LanguagesRouter()
        let interactor = LanguagesInteractor()

        presenter.interactor = interactor
        presenter.router = router
        presenter.view = view
        presenter.direction = direction
        presenter.moduleOutput = moduleOutput

        interactor.output = presenter
        router.transitionHandler = view
        view?.presenter = presenter

        return view ?? LanguagesViewController()
    }
}
