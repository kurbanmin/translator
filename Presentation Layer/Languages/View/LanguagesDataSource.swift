//
//  LanguagesDataSource.swift
//  Translator
//
//  Created by Qurban on 28.04.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class LanguagesDataSource: NSObject {
    var tableView: UITableView
    var frc: NSFetchedResultsController<Language>!
    var coreDataStack = CoreDataStack.shared

    weak var view: LanguagesViewInput!

    init(tableView: UITableView ) {
        self.tableView = tableView
        super.init()

        tableView.register(UINib(nibName: "LanguageCell", bundle: nil), forCellReuseIdentifier: "LanguageCell")
        tableView.dataSource = self
        tableView.delegate = self

        let fetchRequest: NSFetchRequest<Language> = Language.fetchRequest()
        let nameSortDescription = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [nameSortDescription]

        self.frc = NSFetchedResultsController<Language>(fetchRequest: fetchRequest,
                                                        managedObjectContext: coreDataStack.mainContext,
                                                        sectionNameKeyPath: nil,
                                                        cacheName: nil)

        frc.delegate = self
        do {
            try frc.performFetch()

        } catch {
            print(error)
        }
    }
}

extension LanguagesDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.frc.sections else {
            fatalError("no section")
        }
        let sectionInfo = sections[section]

        return sectionInfo.numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let language = self.frc.object(at: indexPath)
        print(language)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as? LanguagesCell
            else {return UITableViewCell()}
        cell.language = language

        return cell
    }
}

extension LanguagesDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let language = self.frc.object(at: indexPath)
        view.didSelect(language: language)
    }
}

extension LanguagesDataSource: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any, at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {

        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        }
    }
}
