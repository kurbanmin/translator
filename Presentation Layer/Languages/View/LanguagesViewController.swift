//
//  LanguagesViewController.swift
//  Translator
//
//  Created by Qurban on 02.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class LanguagesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    var presenter: LanguagesViewOutput!
    var dataSource: LanguagesDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()

       dataSource = LanguagesDataSource(tableView: tableView)
       dataSource?.view = self
    }
}

extension LanguagesViewController: LanguagesViewInput {
    func didSelect(language: Language) {
        presenter?.didSelect(language)
    }
}
