//
//  LanguagesCell.swift
//  Translator
//
//  Created by Qurban on 04.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import UIKit

class LanguagesCell: UITableViewCell {
    @IBOutlet weak var languageLabel: UILabel!

    var language: Language? {
        didSet {
            if let name = language?.name {
                languageLabel.text = name
            }
        }
    }
}
