//
//  Languages.swift
//  Translator
//
//  Created by Qurban on 02.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

class LanguagesRouter {
    weak var transitionHandler: LanguagesViewController!

    func closeCurrentModule() {
        transitionHandler.dismiss(animated: true, completion: nil)
    }
}
