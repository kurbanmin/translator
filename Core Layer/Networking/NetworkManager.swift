//
//  NetworkManager.swift
//  Translator
//
//  Created by Qurban on 28.04.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

protocol INetworkManager {
    func perform<T>(request: URLRequest, completion: @escaping (Result<T>) -> Void) where T: Codable
}

class NetworkManager: INetworkManager {
    func perform<T>(request: URLRequest,
                    completion: @escaping (Result<T>) -> Void
        ) where T: Codable {
        let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
            guard let data = data else {
                completion(Result.error("No Data"))
                return
            }
            do {
                let network = try JSONDecoder().decode(T.self, from: data)
                completion(Result.succes(network))
            } catch {
                completion(Result.error(error.localizedDescription))
            }
        }
        task.resume()
    }
}
