//
//  WordEntityExtension.swift
//  Translator
//
//  Created by Qurban on 25.07.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation
import CoreData

extension Word {
    static func insertWord(from: String,
                           in context: NSManagedObjectContext
        ) -> Word? {

        guard let word = NSEntityDescription.insertNewObject(
            forEntityName: "Word",
            into: context) as? Word
            else {return nil}

        word.from = from
        word.isSaved = false

        return word
    }

    static func findOrInsertWord(from: String,
                                 in context: NSManagedObjectContext
        ) -> Word? {

        var word: Word?
        let fetchRequest: NSFetchRequest<Word> = Word.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "from = %@", from)

        do {
            let results = try context.fetch(fetchRequest)
            assert(results.count < 2, "Multipie words found!")
            if let foundWord = results.first {
                word = foundWord
            }
        } catch {
            print("sdfadsasdsaadfs")
        }

        if word == nil {
            word = insertWord(from: from, in: context)
        }

        return word
    }

    static func deleteWord(from: String,
                           in context: NSManagedObjectContext
        ) -> Word? {

        var word: Word?
        let fetchRequest: NSFetchRequest<Word> = Word.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "from = %@", from)

        do {
            let results = try context.fetch(fetchRequest)
            assert(results.count < 2, "Multipie AppUsers found!")
            if let foundWord = results.first {
                word = foundWord
                if let word = word {
                    context.delete(word)
                }
            }

        } catch {
            print("sdfadsasdsaadfs")

        }
        return word
    }

    static func fetchRequestSavedWords() -> NSFetchRequest<Word> {
        let fetchRequest: NSFetchRequest<Word> = Word.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "isSaved == TRUE")

        return fetchRequest
    }
}
