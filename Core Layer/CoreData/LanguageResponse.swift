//
//  LanguageResponse.swift
//  Translator
//
//  Created by Qurban on 30.04.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation

struct LanguagesResponse: Codable {
    let langs: [String: String]
}
