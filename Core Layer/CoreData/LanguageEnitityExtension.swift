//
//  LanguageEnitityExtension.swift
//  Translator
//
//  Created by Qurban on 05.05.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation
import CoreData

extension Language {

    static func insertLanguage(with code: String,
                               in context: NSManagedObjectContext
        ) -> Language? {
        guard let language = NSEntityDescription.insertNewObject(
            forEntityName: "Language",
            into: context) as? Language
            else {return nil}

        language.code = code
        return language
    }

    static func findOrInsertLanguage(with code: String,
                                     in context: NSManagedObjectContext
        ) -> Language? {

        var language: Language?
        let fetchRequest: NSFetchRequest<Language> = Language.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "code = %@", code)

        do {
            let results = try context.fetch(fetchRequest)
            assert(results.count < 2, "Multipie AppUsers found!")
            if let foundLanguage = results.first {
                language = foundLanguage
            }

        } catch {
            print("sdfadsasdsaadfs")
        }

        if language == nil {
            language = Language.insertLanguage(with: code, in: context)
        }

        return language
    }

    static func fetchRequestLanguages(in context: NSManagedObjectContext) -> NSFetchRequest<Language> {
        let fetchRequest = NSFetchRequest<Language>(entityName: "Language")

        return fetchRequest
    }
}
