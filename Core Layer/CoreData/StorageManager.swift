//
//  StorageManager.swift
//  Translator
//
//  Created by Qurban on 03.05.2019.
//  Copyright © 2019 Qurban. All rights reserved.
//

import Foundation
import CoreData

protocol IStorageManager {
    func updateLanguage(name: String, code: String)
    func deleteWord(from: String)
    func findOrInsertWord(from: String,
                          to: String,
                          completion: @escaping (WordModel) -> Void)
    func update(from: String)
}

class StorageManager: IStorageManager {
    let coreDataStack = CoreDataStack.shared

    var saveContext: NSManagedObjectContext {
        return coreDataStack.saveContext
    }

    var mainContext: NSManagedObjectContext {
        return coreDataStack.mainContext
    }

    func updateLanguage(name: String, code: String) {
        saveContext.perform {
            let language = Language.findOrInsertLanguage(with: code, in: self.saveContext)
            language?.name = name
        }
    }

    func deleteWord(from: String) {
        saveContext.perform {
            _ = Word.deleteWord(from: from, in: self.saveContext)
        }
        coreDataStack.performSave(with: saveContext)
    }

    func findOrInsertWord(from: String,
                          to: String,
                          completion: @escaping (WordModel) -> Void) {
        saveContext.perform {
            if  let word = Word.findOrInsertWord(from: from, in: self.saveContext) {
                word.to = to
                word.from = from
                let wordModel = WordModel(word: word)
                completion(wordModel)
            }
            self.coreDataStack.performSave(with: self.saveContext)
        }
    }

    func update(from: String) {
        saveContext.perform {
            if let word = Word.findOrInsertWord(from: from, in: self.saveContext) {
                word.isSaved = !word.isSaved
            }
            self.coreDataStack.performSave(with: self.saveContext)
        }
    }
}
